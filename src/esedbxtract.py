"""
.       .1111...          | Title: esedbxtract
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Extract hashes from ntds.dit file.
                   ..     | Runs esedbexport as a subprocess and a modified
GrimHacker        ..      | version of dshashes.py.
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
Created on 5 Mar 2014
@author: GrimHacker
esedbxtract extracting hashes from ntds.dit and system files.
    Copyright (C) 2014  Oliver Morton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = "$Revision: 1 $"
# $Source$

# TODO: error handling and logging
#      test with files particularly specifying some or all of ntds, system, datatable, linktable


import os
import logging
import argparse

from lib.esedbexport import ESEDBExport
from lib.dshashes import DSHashes


class HashExtractor(object):
    """Extract hashes from ntds.dit and system files."""
    def __init__(self,
                 system,
                 ntds=None,
                 datatable=None,
                 linktable=None,
                 exclude_disabled=False,
                 passwordhashes=False,
                 passwordhistory=False,
                 esedbexport_exe="esedbexport"):
        """Initialise"""
        self.log = logging.getLogger(__name__)
        self._ntds = ntds
        self._system = system
        self._esedbexport_exe = esedbexport_exe
        self._datatable = datatable
        self._linktable = linktable
        self._passwordhashes = passwordhashes
        self._passwordhistory = passwordhistory
        self._exclude_disabled = exclude_disabled
        self._hashes = []
        self._history = []
        self._workdir = os.getcwd()

    def _get_ntds(self):
        """Return ntds.dit file name."""
        return self._ntds

    def _get_system(self):
        """Return system file name."""
        return self._system

    def _get_esedbexport_exe(self):
        """Return esedbexport executable location."""
        return self._esedbexport_exe

    def _get_datatable(self):
        """Return datatable file name."""
        return self._datatable

    def _get_linktable(self):
        """Return link_table file name."""
        return self._linktable

    def _get_passwordhashes(self):
        """Return password hashes option."""
        return self._passwordhashes

    def _get_passwordhistory(self):
        """Return password history option."""
        return self._passwordhistory

    def _get_exclude_disabled(self):
        """Return exclude disabled accounts option."""
        return self._exclude_disabled

    def get_hashes(self):
        """Return extracted password hashes."""
        return self._hashes

    def _set_hashes(self, hashes):
        """Overwrite hashes list."""
        self._hashes = hashes

    def get_history(self):
        """Return extracted password history hashes."""
        return self._history

    def _set_history_hashes(self, hashes):
        """Overwrite history hashes list."""
        self._history = hashes

    def _get_workdir(self):
        """Return working directory."""
        return self._workdir

    def _export_tables(self):
        """Run esedbexport to get data and link tables."""
        # TODO: move existing datatable and linktable export to .old
        #self.log.debug("Creating ESEDBExport instance.")
        try:
            esedbexport = ESEDBExport(datatable=self._get_datatable(),
                                      linktable=self._get_linktable(),
                                      ntds=self._get_ntds(),
                                      exe=self._get_esedbexport_exe(),
                                      workdir=self._get_workdir())
        except Exception as e:
            self.log.critical("Failed to create instance of ESEDBExport. {0}".format(e))
            exit()
        else:
            self.log.debug("Extracting tables...")
            try:
                esedbexport.extract()
            except Exception as e:
                self.log.critical("Failed to extract tables. {0}".format(e))
                exit()
            else:
                self.log.debug("Getting datatable name...")
                try:
                    self._datatable = esedbexport.get_datatable()
                except Exception as e:
                    self.log.critical("Failed to get datatable filename. {0}".format(e))
                    exit()
                else:
                    self.log.debug("Getting linktable name...")
                    try:
                        self._linktable = esedbexport.get_linktable()
                    except Exception as e:
                        self.log.critical("Failed to get link_table filename. {0}".format(e))
                        exit()

    def _extract_hashes(self):
        """Extract hashes from data amd link tables and system file."""
        #self.log.debug("Creating DSHashes instance.")
        try:
            dshashes = DSHashes(datatable=self._get_datatable(),
                                linktable=self._get_linktable(),
                                system=self._get_system(),
                                passwordhashes=self._get_passwordhashes(),
                                passwordhistory=self._get_passwordhistory(),
                                exclude_disabled=self._get_exclude_disabled(),
                                workdir=self._get_workdir())
        except Exception as e:
            self.log.critical("Failed to create instance of DSHashes. {0}".format(e))
            exit()
        else:
            self.log.debug("Extracting hashes with dshashes...")
            try:
                dshashes.run()
            except Exception as e:
                self.log.critical("Failed to run DSHashes. {0}".format(e))
                exit()
            else:
                self.log.debug("Getting hashes from dshashes...")
                try:
                    self._set_hashes(dshashes.get_hashes())
                except Exception as e:
                    self.log.critical("Failed to get password hashes. {0}".format(e))
                    exit()
                else:
                    self.log.debug("Getting history hashes from dshashes...")
                    try:
                        self._set_history_hashes(dshashes.get_history())
                    except Exception as e:
                        self.log.critical("Failed to get password history hashes. {0}".format(e))
                        exit()

    def run(self):
        """Export tables and extract."""
        if (self._get_datatable() is None or self._get_linktable() is None) and self._get_ntds() is not None:
            self.log.info("Exporting tables. This can take minutes or hours depending on the size of the Active Directory...")
            try:
                self._export_tables()
            except Exception as e:
                self.log.critical("Error exporting tables. {0}".format(e))
                exit()
        else:
            self.log.warning("datatable and linktable will not be exported. Either datatable and linktable are specified or ntds.dit file is not specified.")

        if self._get_passwordhashes() or self._get_passwordhistory():
            # Instructed to extract either current or historical hashes or both
            if self._get_datatable() is not None and \
                    self._get_linktable() is not None and \
                    self._get_system() is not None:
                # The three required files are specified.
                # TODO: Check they exist??
                self.log.info("Extracting hashes. This might take few minutes or so...")
                try:
                    self._extract_hashes()
                except Exception as e:
                    self.log.critical("Error extracting hashes. {0}".format(e))
                    exit()
            else:
                self.log.critical("datatable, linktable and system must be specified to extract hashes.")
        else:
            self.log.critical("Instructed not to extract current or historical hashes. Nothing to do.")


class Reporter(object):
    """Create output files."""
    def __init__(self, hashes, filename):
        """Initialise"""
        self.log = logging.getLogger(__name__)
        self._hashes = hashes
        self._filename = filename

    def _get_hashes(self):
        """Return hashes"""
        return self._hashes

    def _get_filename(self):
        """Return filename"""
        return self._filename

    def generate(self):
        """Write output file."""
        self.log.debug("Writing hashes to: {0}".format(self._get_filename()))
        try:
            with open(self._get_filename(), 'w') as f:
                for hash_ in self._get_hashes():
                    try:
                        f.writelines("{0}\n".format(hash_))
                    except Exception as e:
                        self.log.critical("Failed to write hash to file. {0}".format(e))
        except Exception as e:
            self.log.critical("Failed to write file: {0}. {1}".format(self._get_filename(), e))
            self.log.info("Written hashes to: {0}".format(self._get_filename()))


if __name__ == '__main__':
    def main(args):
        extractor = HashExtractor(datatable=args.datatable,
                                  linktable=args.linktable,
                                  system=args.system,
                                  ntds=args.ntds,
                                  esedbexport_exe=args.esedbexport,
                                  exclude_disabled=args.exclude_disabled,
                                  passwordhashes=args.passwordhashes,
                                  passwordhistory=args.historyhashes)
        extractor.run()

        if args.passwordhashes:
            if len(extractor.get_hashes()) != 0:
                reporter = Reporter(filename=args.passwordhashesout, hashes=extractor.get_hashes())
                reporter.generate()
                log.info("Password hashes written to '{0}'".format(args.passwordhashesout))
            else:
                log.warning("No password hashes extracted.")

        if args.historyhashes:
            if len(extractor.get_history()) != 0:
                reporter = Reporter(filename=args.historyhashesout, hashes=extractor.get_history())
                reporter.generate()
                log.info("Password history hashes written to '{0}'".format(args.historyhashesout))
            else:
                log.warning("No password history hashes extracted.")

    def print_version():
        """Print command line version banner."""
        print """.       .1111...          | Title: esedbxtract {0}
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description: Extract hashes from ntds.dit file.
                    ..    | Runs esedbexport as a subprocess and a modified
                   ..     | version of dshashes.py.
GrimHacker        ..      | This program comes with ABSOLUTELY NO WARRANTY;
                 ..       | for details type `show w'. This is free software,
grimhacker.com  ..        | and you are welcome to redistribute it under
@grimhacker    ..         | certain conditions.
----------------------------------------------------------------------------
""".format(__version__)

    print_version()

    parser = argparse.ArgumentParser(description="Extract hashes from ntds.dit file.")
    parser.add_argument("--verbose", help="Verbose logging.", action="store_true")
    parser.add_argument("-p", "--passwordhashes", help="Don't extract password hashes", action="store_false")
    parser.add_argument("-P", "--historyhashes", help="Extract password history hashes", action="store_true")
    parser.add_argument("-e", "--exclude_disabled", help="Exclude disabled accounts", action="store_true")
    inputfiles = parser.add_argument_group()
    inputfiles.add_argument("-d", "--datatable", help="datatable file", default=None)
    inputfiles.add_argument("-l", "--linktable", help="linktable file", default=None)
    inputfiles.add_argument("-s", "--system", help="SYSTEM Hive file", default=None)
    inputfiles.add_argument("-n", "--ntds", help="NTDS.DIT file", default=None)
    outputfiles = parser.add_argument_group()
    outputfiles.add_argument("-o", "--passwordhashesout", help="password hash pwdump file", default="hashes.pwdump")
    outputfiles.add_argument("-O", "--historyhashesout", help="history hash pwdump file", default="history.pwdump")
    parser.add_argument("--esedbexport", help="esedbexport executable (if not in PATH)", default="esedbexport")

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format="%(levelname)s: %(module)s: %(message)s")
    log = logging.getLogger(__name__)


    if (args.ntds or (args.datatable and args.linktable) or (args.ntds and args.datatable) or (args.ntds and args.linktable)) and not (args.ntds and args.datatable and args.linktable):
        if (args.passwordhashes or args.historyhashes) and not args.system:
            log.critical("Must specify SYSTEM to extract hashes.")
            parser.print_usage()
            exit()
        main(args)
    else:
        log.critical("Must specify (ntds) or (datatable and linktable) or (ntds and datatable) or (ntds and linktable), but not all.")
        parser.print_usage()
        exit()
