# This file was derived from dshashes.py by LaNMaSteR53
# This file was derived from dsusers.py which is is part of ntdsxtract.
#
# ntdsxtract is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ntdsxtract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ntdsxtract.  If not, see <http://www.gnu.org/licenses/>.

'''
@editor:        GrimHacker
@editor:        LaNMaSteR53
@author:        Csaba Barta
@license:       GNU General Public License 2.0 or later
@contact:       csaba.barta@gmail.com
'''
'''
Refactored code into a class so that it can be more easily encorporated
into other scripts.
Changed the command line input to argparse.
Output and options are practically the same as dshashes.py by LaNMaSteR53,
other than --passwordhashes and --passwordhistory are now flags and the 
system hive is specified using --system.
Updated calls to ntsxtract functions to work with NTDSxtract v1.2b.
    - GrimHacker (5 Mar 2014)
'''


import argparse
import logging

from lib.ntdsxtract.ntds.dsdatabase import *
from lib.ntdsxtract.ntds.dsrecord import *
from lib.ntdsxtract.ntds.dslink import *
from lib.ntdsxtract.ntds.dstime import *
from lib.ntdsxtract.ntds.dsobjects import *


class Error(Exception):
    """Base class for exceptions raised by this module."""
    pass


class InputError(Error):
    """Exception raised for errors in the input.
    Attributes:
        msg  -- explanation of the error
    """
    def __init__(self, msg):
        self.msg = msg

class NTDSXTRACTError(Error):
    """Exception raised for errors in the input.
    Attributes:
        msg  -- explanation of the error
    """
    def __init__(self, msg):
        self.msg = msg


class DSHashes(object):
    """Extracts user hashes in a user friendly format.
    Derived from dshashes.py which is derived from dsusers.py which is part of ntdsxtract."""
    def __init__(self,
                datatable,
                linktable,
                passwordhashes=False,
                passwordhistory=False,
                exclude_disabled=False,
                rid=None,
                name=None,
                system=None,
                workdir="."):
        """Initialise DSHashes class."""
        self.log = logging.getLogger(__name__)
        if (passwordhashes or passwordhistory) and not system:
            raise InputError("Must supply the system hive to extract password hashes or password history.")
        else:
            self._passwordhashes = passwordhashes
            self._passwordhistory = passwordhistory
            self._exclude_disabled = exclude_disabled
            self._rid = rid
            self._name = name
            self._hashes = []
            self._history = []
            self._workdir = workdir
            self.log.debug("Initalising database...")
            try:
                self._db = dsInitDatabase(datatable, self._workdir)
            except Exception as e:
                self.log.critical("Failed to initialise database. {0}".format(e))
                #TODO: Try and handle here
                raise
            else:
                self.log.debug("Initialising links...")
                try:
                    self._dl = dsInitLinks(linktable, self._workdir)
                except Exception as e:
                    self.log.critical("Failed to initialise links. {0}".format(e))
                    #TODO: Try and handle here
                    raise
                else:
                    if system:
                        self.log.debug("SYSTEM specified. Initalising encryption...")
                        try:
                            dsInitEncryption(system)
                        except Exception as e:
                            self.log.critical("Failed to initialise encryption. {0}".format(e))
                            #TODO: Try and handle here
                            raise

    def get_hashes(self):
        """Return hashes."""
        return self._hashes

    def _set_hash(self, hash):
        """Add hash to list of hashes."""
        self._hashes.append(hash)

    def get_history(self):
        """Return history."""
        return self._history

    def _set_history_hash(self, hash_):
        """Add hash to list of history hashes."""
        self._history.append(hash_)

    def _get_db(self):
        """Return database."""
        return self._db

    def _get_dl(self):
        """Return links."""
        return self._dl

    def _get_passwordhashes(self):
        """Return passwordhashes."""
        return self._passwordhashes

    def _get_passwordhistory(self):
        """Return passwordhistory."""
        return self._passwordhistory

    def _get_exclude_disabled(self):
        """Return exclude_disabled."""
        return self._exclude_disabled

    def _get_rid(self):
        """Return rid."""
        return self._rid

    def _get_name(self):
        """Return name."""
        return self._name

    def _get_person(self):
        """Use dsGetTypeIdByTypeName to get type id for 'Person'.
        Return type id."""
        self.log.debug("Getting 'Person' type ID...")
        try:
            utype = dsGetTypeIdByTypeName(self._get_db(), "Person")
        except Exception as e:
            raise NTDSXTRACTError("Error getting type id for Person. {0}".format(e))
        else:
            # TODO: Check if this can be handled in a try/except block.
            if utype == -1:  # dsGetTypeIdByTypeName returns -1 when an error occured.
                #T ODO: Try and handle here
                raise NTDSXTRACTError("Unable to get type id for Person")
            else:
                return utype

    def _retrieve_hashes(self, user):
        """Retrieve password hashes."""
        self.log.debug("Retrieving password hashes...")
        nthash = '31d6cfe0d16ae931b73c59d7e0c089c0'  # ntlm hash for an empty password
        lmhash = 'aad3b435b51404eeaad3b435b51404ee'  # lm hash for an empty password
        try:
            (lm, nt) = user.getPasswordHashes()
        except Exception as e:
            raise NTDSXTRACTError("Error getting password hashes. {0}".format(e))
        else:
            if nt != '':
                nthash = nt
                if lm != '':
                    lmhash = lm
            try:
                hash_ = "{0}:{1}:{2}:{3}:::".format(user.SAMAccountName, user.SID.RID, lmhash, nthash)
            except Exception as e:
                raise NTDSXTRACTError("Error creating hash as a string. {0}".format(e))
            else:
                if nt != '':
                    self._set_hash(hash_)

    def _retrieve_history(self, user):
        """Retrieve password history hashes."""
        self.log.debug("Retrieving password history hashes...")
        lmhistory = None
        nthistory = None
        try:
            (lmhistory, nthistory) = user.getPasswordHistory()
        except Exception as e:
            raise NTDSXTRACTError("Error getting password history hashes. {0}".format(e))
        else:
            if nthistory is not None:
                try:
                    for hashid, nthash in enumerate(nthistory):
                        try:
                            self._set_history_hash("{0}_nthistory{1}:{2}:E52CAC67419A9A224A3B108F3FA6CB6D:{3}:::".format(user.SAMAccountName, hashid, user.SID.RID, nthash))
                            # E52CAC67419A9A224A3B108F3FA6CB6D is the LM hash of 'password'
                            # This is done in order to make sure the output plays nice with various hash cracking tools. Account for this when cracking historical hashes.
                        except Exception as e:
                            raise NTDSXTRACTError("Error creating nt history hash as a string. {0}".format(e))
                except Exception as e:
                    NTDSXTRACTError("Error looping over nthistory. {0}".format(e))
                if lmhistory is not None:
                    try:
                        for hashid, lmhash in enumerate(lmhistory):
                            try:
                                self._set_history_hash("{0}_lmhistory{1}:{2}:{3}:8846F7EAEE8FB117AD06BDD830B7586C:::".format(user.SAMAccountName, hashid, user.SID.RID, lmhash))
                                # 8846F7EAEE8FB117AD06BDD830B7586C is the NTLM hash of 'password'
                                # This is done in order to make sure the output plays nice with various hash cracking tools. Account for this when cracking historical hashes.
                            except Exception as e:
                                raise NTDSXTRACTError("Error creating lm history hash as a string. {0}".format(e))
                    except Exception as e:
                        NTDSXTRACTError("Error looping over lmhistory. {0}".format(e))

    def run(self):
        """Run"""
        self.log.debug("Running dshashes...")
        for recordid in dsMapLineIdByRecordId:
            try:
                if int(dsGetRecordType(self._get_db(), recordid)) == self._get_person():
                    try:
                        user = dsUser(self._get_db(), recordid)
                    except Exception as e:
                        NTDSXTRACTError("Error using dsUser. {0}".format(e))
                    else:
                        try:
                            if self._get_rid() is not None and user.SID.RID != self._get_rid():
                                continue  # If the user specificed a rid, and this isn't it, continue to the next iternation.
                        except Exception as e:
                            raise NTDSXTRACTError("Error getting sid/rid. {0}".format(e))
                        else:
                            try:
                                if self._get_name() is not None and user.Name != self._get_name():
                                    continue  # If the user specificed a name, and this isn't it, continue to the next iternation.
                            except Exception as e:
                                raise NTDSXTRACTError("Error getting name. {0}".format(e))
                            else:
                                try:
                                    if self._get_exclude_disabled():
                                        try:
                                            for uac in user.getUserAccountControl():
                                                if uac == 'Disabled':
                                                    continue  # If the user specified to skip disabled accounts, and this account is disabled, continue to the next iteration.
                                        except Exception as e:
                                            raise NTDSXTRACTError("Error using user.getUserAccountControl(). {0}".format(e))
                                except Exception as e:
                                    raise NTDSXTRACTError("Error getting disabled flag. {0}".format(e))
                                else:
                                    try:
                                        if self._get_passwordhashes():  # If the user specified to retrieve password hashes.
                                            try:
                                                self._retrieve_hashes(user)
                                            except Exception as e:
                                                raise NTDSXTRACTError("Error retrieving hashes. {0}".format(e))
                                    except Exception as e:
                                        raise NTDSXTRACTError("Error getting passwordhashes flag. {0}".format(e))

                                    try:
                                        if self._get_passwordhistory():  # If the user specified to retrieve password history hashes.
                                            try:
                                                self._retrieve_history(user)
                                            except Exception as e:
                                                raise NTDSXTRACTError("Error retrieving history hashes. {0}".format(e))
                                    except Exception as e:
                                        raise NTDSXTRACTError("Error getting passwordhistory flag. {0}".format(e))
            except Exception as e:
                raise NTDSXTRACTError("Error using dsGetRecordType. {0}".format(e))
            finally:
                self.log.debug("Finished running dshashes.")

        '''NOTE: NT and LM hashes are shown on individual lines with the respective hash of 'password' in the opposing position."
        This is done in order to make sure the output plays nice with various hash cracking tools. Account for this when cracking historical hashes.'''

if __name__ == '__main__':
    """Act like dshashes.py by LaNMaSteR53 - retrieve and print hashes.
    Differences: 'passwordhashes' and 'passwordhistory' are flags.
                 'system' option added to specify SYSTEM hive.
                 This is because argparse can't have two options with the same destination variable.
                 'verbose' option for logging."""
    parser = argparse.ArgumentParser(description="Extracts user hashes in a user-friendly format.")
    parser.add_argument("datatable")
    parser.add_argument("linktable")
    parser.add_argument("--rid", help="List user identified by RID")
    parser.add_argument("--name", help="List user identified by Name")
    parser.add_argument("--passwordhashes", help="Extract password hashes. Must supply system hive.", action="store_true")
    parser.add_argument("--passwordhistory", help="Extract password history. Must supply system hive.", action="store_true")
    parser.add_argument("--system", help="SYSTEM hive. Required for passwordhashes and passwordhistory.")
    parser.add_argument("--exclude-disabled", help="Exclude disabled accounts from output", action="store_true")
    parser.add_argument("--verbose", help="Verbose logging.", action="store_true")

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format="%(levelname)s: %(module)s: %(message)s")
    log = logging.getLogger(__name__)

    if (args.passwordhashes or args.passwordhistory) and not args.system:
        log.critical("Must supply the system hive to extract password hashes or password history.")
        exit()
    elif args.system and not (args.passwordhashes or args.passwordhistory):
        log.critical("System hive is only required for password hashes and password history. Ignoring.")
        args.system = None
    else:
        dshashes = DSHashes(datatable=args.datatable,
                            linktable=args.linktable,
                            passwordhashes=args.passwordhashes,
                            passwordhistory=args.passwordhistory,
                            exclude_disabled=args.exclude_disabled,
                            rid=args.rid,
                            name=args.name,
                            system=args.system)
        dshashes.run()

        if args.rid:
            print "\tUser RID: {0}".format(args.rid)  # If user has specified a RID, print it.

        if args.name:
            print "\tUser name: {0}".format(args.name)  # If user has specified a name, print it.

        print "\nList of hashes:"
        print "=============="
        try:
            if args.passwordhashes:
                try:
                    for hash_ in dshashes.get_hashes():
                        try:
                            print hash_
                        except Exception as e:
                            log.critical("Failed to print hash. {0}".format(e))
                except Exception as e:
                    log.critical("Error getting hashes to print. {0}".format(e))
        except Exception as e:
            log.critical("Error getting args.passwordhashes. {0}".format(e))

        try:
            if args.passwordhistory:
                try:
                    for hash_ in dshashes.get_history():
                        try:
                            print hash_
                        except Exception as e:
                            log.critical("Failed to print history hash. {0}".format(e))
                except Exception as e:
                    log.critical("Error getting history hashes to print. {0}".format(e))
        except Exception as e:
            log.critical("Error getting args.passwordhistory. {0}".format(e))
        else:
            print "\n[*] NOTE: NT and LM hashes are shown on individual lines with the respective hash of 'password' in the opposing position."
            print "This is done in order to make sure the output plays nice with various hash cracking tools. Account for this when cracking historical hashes.\n"
