# This file is part of ntdsxtract.
#
# ntdsxtract is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ntdsdump is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ntdsdump.  If not, see <http://www.gnu.org/licenses/>.

'''
@author:        Csaba Barta
@license:       GNU General Public License 2.0 or later
@contact:       csaba.barta@gmail.com
'''
import sys
import time
from operator import *
from ntds.version import *
from ntds.dsdatabase import *
from ntds.dsobjects import *
from ntds.dstime import *
from ntds.dsrecord import *
from ntds.lib.fs import *
from ntds.lib.csvoutput import *

times = []
timeline = []

def usage():
    sys.stderr.write("\nDSTimeline v" + str(ntds.version.version))
    sys.stderr.write("\nConstructs timeline")
    sys.stderr.write("\n\nusage: %s <datatable> <work directory> [option]" % sys.argv[0])
    sys.stderr.write("\n\n  datatable")
    sys.stderr.write("\n    The full path to the file called datatable extracted by esedbexport")
    sys.stderr.write("\n  work directory")
    sys.stderr.write("\n    The full path to the directory where ntdsxtract should store its")
    sys.stderr.write("\n    cache files and output files. If the directory does not exist")
    sys.stderr.write("\n    it will be created.")
    sys.stderr.write("\n  options:")
    sys.stderr.write("\n    --b")
    sys.stderr.write("\n       Output timeline in mactime body format. If this option is selected the")
    sys.stderr.write("\n       option csvoutfile will be ignored")
    sys.stderr.write("\n    --csvoutfile <name of the CSV output file>")
    sys.stderr.write("\n          The filename of the csv file to which ntdsxtract should write the")
    sys.stderr.write("\n          output")
    sys.stderr.write("\n\nFields of the default output")
    sys.stderr.write("\n    Timestamp|Action|Record ID|Obj. name|Obj. type")
    sys.stderr.write("\n")

if len(sys.argv) < 2:
    usage()
    sys.exit(1)

sys.stderr.write("\n[+] Started at: %s" % time.strftime(
                                        "%a, %d %b %Y %H:%M:%S UTC",
                                        time.gmtime()))

csvoutfile = ""
sys.stderr.write("\n[+] Started with options:")
optid = 0
for opt in sys.argv:
    if opt == "--b":
        sys.stderr.write("\n\t[-] Using mactime body format")
    if opt == "--csvoutfile":
        if len(sys.argv) < optid + 2:
            usage()
            sys.exit(1)
        csvoutfile = sys.argv[optid + 1]
        sys.stderr.write("\n\t[-] CSV output filename: " + sys.argv[optid + 1])
    optid += 1

# Setting up the environment
if not checkfile(sys.argv[1]):
    print("\n[!] Error! datatable cannot be found!")
    sys.exit()
wd = ensure_dir(sys.argv[2])

if csvoutfile != "":
    init_csv(path.join(wd, csvoutfile))

# Initialize engine
db = dsInitDatabase(sys.argv[1], wd)

if csvoutfile != "":
    write_csv(["Timestamp", "Event", "Record ID", "Object name",
                   "Object type"
            ])

i = 0
l = len(dsMapLineIdByRecordId)
for recordid in dsMapLineIdByRecordId:
    sys.stderr.write("\r[+] Building timeline - %d%% -> %d records processed" % (
                                                                           i*100/l,
                                                                           i
                                                                           ))
    sys.stderr.flush()
    try:
        tmp = dsObject(db, recordid)
    except:
        continue
    
    if len(sys.argv) == 4 and sys.argv[3] == "--b":
        if tmp.WhenChanged != -1 or tmp.WhenCreated != -1:
            times.append((recordid, 
                          0 if tmp.WhenCreated == -1 else tmp.WhenCreated, 
                          0 if tmp.WhenChanged == -1 else tmp.WhenChanged, 
                          tmp.Name,
                          tmp.Type,
                          ""
                          ))
        
        if tmp.Type == "Person":
            user = dsAccount(db, recordid)
            if user.LastLogon != -1:
                times.append((recordid, 
                              0, 
                              user.LastLogon, 
                              user.Name,
                              user.Type, 
                              "Logged in"
                              ))
            if user.LastLogonTimeStamp != -1:
                times.append((recordid, 
                              0, 
                              user.LastLogonTimeStamp, 
                              user.Name,
                              user.Type, 
                              "Login timestamp sync"
                              ))
            if user.PasswordLastSet != -1:
                times.append((recordid, 
                              0, 
                              user.PasswordLastSet, 
                              user.Name, 
                              user.Type,
                              "Password changed"
                              ))
            user = None
    
    else:
        if dsVerifyDSTimeStamp(tmp.WhenCreated) != -1:
            times.append((recordid, tmp.WhenCreated, "Created", tmp.Name, tmp.Type))
        if dsVerifyDSTimeStamp(tmp.WhenChanged) != -1:
            times.append((recordid, tmp.WhenChanged, "Modified", tmp.Name, tmp.Type))
        
        if tmp.Type == "Person":
            user = dsAccount(db, recordid)
            if user.LastLogon != -1 and user.LastLogon != 0:
                times.append((recordid,
                              user.LastLogon, 
                              "Logged in", 
                              user.Name, 
                              user.Type
                              ))
                
            if user.LastLogonTimeStamp != -1 and user.LastLogonTimeStamp != 0:
                times.append((recordid, 
                              user.LastLogonTimeStamp, 
                              "Login timestamp sync", 
                              user.Name, 
                              user.Type
                              ))
                
            if user.PasswordLastSet != -1 and user.PasswordLastSet != 0:
                times.append((recordid, 
                              user.PasswordLastSet, 
                              "Password changed", 
                              user.Name, 
                              user.Type
                              ))
            user = None
    i += 1
sys.stderr.write("\n")
        
timeline = sorted(times, key=itemgetter(1))
for item in timeline:
    if len(sys.argv) == 4 and sys.argv[3] == "--b":
        (id, ctimestamp, mtimestamp, name, type, actiontype) = item
        if actiontype != "":
            sys.stdout.write("\n0|%s (%s) - (%s)|%d||0|0|0|0|%d|0|%d" % (
                                                     name, 
                                                     type,
                                                     actiontype, 
                                                     id,
                                                     dsGetPOSIXTimeStamp(mtimestamp),
                                                     dsGetPOSIXTimeStamp(ctimestamp)
                                                     ))
        else:
            sys.stdout.write("\n0|%s (%s)|%d||0|0|0|0|%d|0|%d" % (
                                                     name, 
                                                     type, 
                                                     id,
                                                     dsGetPOSIXTimeStamp(mtimestamp),
                                                     dsGetPOSIXTimeStamp(ctimestamp)
                                                     ))
    else:
        (id, timestamp, action, name, type) = item
        sys.stdout.write("\n%s|%s|%d|%s (%s)" % (
                                       dsGetDSTimeStampStr(timestamp),
                                       action,
                                       id,
                                       name,
                                       type
                                       ))
        write_csv(["=\"" + dsGetDSTimeStampStr(timestamp) + "\"", action, id, name,
                   type
            ])
        
sys.stdout.write("\n")
sys.stdout.flush()