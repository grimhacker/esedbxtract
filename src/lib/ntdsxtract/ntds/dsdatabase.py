# This file is part of ntdsxtract.
#
# ntdsxtract is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ntdsxtract is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ntdsxtract.  If not, see <http://www.gnu.org/licenses/>.

'''
@author:        Csaba Barta
@license:       GNU General Public License 2.0 or later
@contact:       csaba.barta@gmail.com
'''

import sys
from stat import *
from os import stat
from os import path
import time
import dsfielddictionary
from dsencryption import *
from lib.map import *
import pickle

dsMapOffsetByLineId   = {} #Map that can be used to find the offset for line
dsMapLineIdByRecordId = {} #Map that can be used to find the line for record
dsMapTypeByRecordId   = {} #Map that can be used to find the type for record
dsMapRecordIdByName   = {} #Map that can be used to find the record for name
dsMapChildsByRecordId = {} #Map that can be used to find child objects
dsMapTypeIdByTypeName = {} #Map that can be used to find child objects

dsDatabaseSize = -1

#def dsLoadMap(filename, map):
#    fmap = open(filename, "rb")
#    tmp = {}
#    tmp = pickle.load(fmap)
#    fmap.close()
#    for id in tmp:
#        map[id] = tmp[id]

def dsInitDatabase(dsESEFile, workdir):
    global dsDatabaseSize
    dsDatabaseSize = stat(dsESEFile).st_size
    sys.stderr.write("\n[+] Initialising engine...\n")  
    db = open(dsESEFile , 'rb', 0)
    db.seek(0)
    line = db.readline()
    if line == "":
        sys.stderr.write("[!] Warning! Error processing the first line!\n")
        sys.exit()
    else:
        dsFieldNameRecord = line.split('\t')
        record = line.split('\t')
        for cid in range(0, len(record)-1):
#------------------------------------------------------------------------------ 
# filling indexes for object attributes
#------------------------------------------------------------------------------ 
            if (record[cid] == "DNT_col"):
                dsfielddictionary.dsRecordIdIndex = cid
            if (record[cid] == "PDNT_col"):
                dsfielddictionary.dsParentRecordIdIndex = cid
            if (record[cid] == "time_col"):
                dsfielddictionary.dsRecordTimeIndex = cid
            if (record[cid] == "Ancestors_col"):
                dsfielddictionary.dsAncestorsIndex = cid
            if (record[cid] == "ATTb590606"):
                dsfielddictionary.dsObjectTypeIdIndex = cid
            if (record[cid] == "ATTm3"):
                dsfielddictionary.dsObjectNameIndex = cid
            if (record[cid] == "ATTm589825"):
                dsfielddictionary.dsObjectName2Index = cid
            if (record[cid] == "ATTk589826"):
                dsfielddictionary.dsObjectGUIDIndex = cid
            if (record[cid] == "ATTl131074"):
                dsfielddictionary.dsWhenCreatedIndex = cid
            if (record[cid] == "ATTl131075"):
                dsfielddictionary.dsWhenChangedIndex = cid
            if (record[cid] == "ATTq131091"):
                dsfielddictionary.dsUSNCreatedIndex = cid
            if (record[cid] == "ATTq131192"):
                dsfielddictionary.dsUSNChangedIndex = cid
            if (record[cid] == "OBJ_col"):
                dsfielddictionary.dsObjectColIndex = cid
            if (record[cid] == "ATTi131120"):
                dsfielddictionary.dsIsDeletedIndex = cid
#------------------------------------------------------------------------------ 
# Filling indexes for deleted object attributes
#------------------------------------------------------------------------------ 
            if (record[cid] == "ATTb590605"):
                dsfielddictionary.dsOrigContainerIdIndex = cid
#------------------------------------------------------------------------------ 
# Filling indexes for account object attributes
#------------------------------------------------------------------------------ 
            if (record[cid] == "ATTr589970"):
                dsfielddictionary.dsSIDIndex = cid
            if (record[cid] == "ATTm590045"):
                dsfielddictionary.dsSAMAccountNameIndex = cid
            if (record[cid] == "ATTm590480"):
                dsfielddictionary.dsUserPrincipalNameIndex = cid
            if (record[cid] == "ATTj590126"):
                dsfielddictionary.dsSAMAccountTypeIndex = cid
            if (record[cid] == "ATTj589832"):
                dsfielddictionary.dsUserAccountControlIndex = cid
            if (record[cid] == "ATTq589876"):
                dsfielddictionary.dsLastLogonIndex = cid
            if (record[cid] == "ATTq591520"):
                dsfielddictionary.dsLastLogonTimeStampIndex = cid
            if (record[cid] == "ATTq589983"):
                dsfielddictionary.dsAccountExpiresIndex = cid
            if (record[cid] == "ATTq589920"):
                dsfielddictionary.dsPasswordLastSetIndex = cid
            if (record[cid] == "ATTq589873"):
                dsfielddictionary.dsBadPwdTimeIndex = cid
            if (record[cid] == "ATTj589993"):
                dsfielddictionary.dsLogonCountIndex = cid
            if (record[cid] == "ATTj589836"):
                dsfielddictionary.dsBadPwdCountIndex = cid
            if (record[cid] == "ATTj589922"):
                dsfielddictionary.dsPrimaryGroupIdIndex = cid
            if (record[cid] == "ATTk589914"):    
                dsfielddictionary.dsNTHashIndex = cid
            if (record[cid] == "ATTk589879"):
                dsfielddictionary.dsLMHashIndex = cid
            if (record[cid] == "ATTk589918"):
                dsfielddictionary.dsNTHashHistoryIndex = cid
            if (record[cid] == "ATTk589984"):
                dsfielddictionary.dsLMHashHistoryIndex = cid
            if (record[cid] == "ATTk591734"):
                dsfielddictionary.dsUnixPasswordIndex = cid
            if (record[cid] == "ATTk36"):
                dsfielddictionary.dsADUserObjectsIndex = cid
            if (record[cid] == "ATTk589949"):
                dsfielddictionary.dsSupplementalCredentialsIndex = cid
#------------------------------------------------------------------------------
# Filling indexes for computer objects attributes
#------------------------------------------------------------------------------
            if (record[cid] == "ATTj589993"):
                dsfielddictionary.dsLogonCountIndex = cid
            if (record[cid] == "ATTm590443"):
                dsfielddictionary.dsDNSHostNameIndex = cid
            if (record[cid] == "ATTm590187"):
                dsfielddictionary.dsOSNameIndex = cid
            if (record[cid] == "ATTm590188"):
                dsfielddictionary.dsOSVersionIndex = cid
#------------------------------------------------------------------------------ 
# Filling indexes for bitlocker objects
#------------------------------------------------------------------------------ 
            if (record[cid] == "ATTm591788"):
                dsfielddictionary.dsRecoveryPasswordIndex = cid
            if (record[cid] == "ATTk591823"):
                dsfielddictionary.dsFVEKeyPackageIndex = cid
            if (record[cid] == "ATTk591822"):
                dsfielddictionary.dsVolumeGUIDIndex = cid
            if (record[cid] == "ATTk591789"):
                dsfielddictionary.dsRecoveryGUIDIndex = cid
#===============================================================================
# Filling indexes for AD encryption
#===============================================================================
            if (record[cid] == "ATTk590689"):
                dsfielddictionary.dsPEKIndex = cid
    db.seek(0)
    dsCheckMaps(db, workdir)
#    dsBuildMaps(db)
    return db

def dsCheckMaps(dsDatabase, workdir):
    try:
        global dsMapOffsetByLineId
        global dsMapLineIdByRecordId
        global dsMapRecordIdByName
        global dsMapTypeByRecordId
        global dsMapChildsByRecordId
        global dsMapTypeIdByTypeName

        sys.stderr.write("[+] Loading saved map files (Stage 1)...\n")
        dsLoadMap(path.join(workdir, "offlid.map"), dsMapOffsetByLineId)
        dsLoadMap(path.join(workdir, "lidrid.map"), dsMapLineIdByRecordId)
        dsLoadMap(path.join(workdir, "ridname.map"), dsMapRecordIdByName)
        dsLoadMap(path.join(workdir, "typerid.map"), dsMapTypeByRecordId)
        dsLoadMap(path.join(workdir, "childsrid.map"), dsMapChildsByRecordId)
        dsLoadMap(path.join(workdir, "typeidname.map"), dsMapTypeIdByTypeName)
        
        pek = open(path.join(workdir, "pek.map"), "rb")
        dsfielddictionary.dsEncryptedPEK = pek.read()
        pek.close()
        
    except Exception as e:
        sys.stderr.write("[!] Warning: Opening saved maps failed: " + str(e) + "\n")
        sys.stderr.write("[+] Rebuilding maps...\n")
        dsBuildMaps(dsDatabase, workdir)
        pass

def dsBuildMaps(dsDatabase, workdir):
    
    global dsMapOffsetByLineId
    global dsMapLineIdByRecordId
    global dsMapRecordIdByName
    global dsMapTypeByRecordId
    global dsMapChildsByRecordId
        
    lineid = 0
    while True:
        sys.stderr.write("\r[+] Scanning database - %d%% -> %d records processed" % (
                                            dsDatabase.tell()*100/dsDatabaseSize,
                                            lineid+1
                                            ))
        sys.stderr.flush()
        try:
            dsMapOffsetByLineId[lineid] = dsDatabase.tell()
        except:
            sys.stderr.write("\n[!] Warning! Error at dsMapOffsetByLineId!\n")
            pass
        line = dsDatabase.readline()
        if line == "":
            break
        record = line.split('\t')
        if lineid != 0:
            #===================================================================
            # This record will always be the record representing the domain
            # object
            #===================================================================
            if record[dsfielddictionary.dsPEKIndex] != "":
                dsfielddictionary.dsEncryptedPEK = record[dsfielddictionary.dsPEKIndex]
            try:
                dsMapLineIdByRecordId[int(record[dsfielddictionary.dsRecordIdIndex])] = lineid
            except:
                sys.stderr.write("\n[!] Warning! Error at dsMapLineIdByRecordId!\n")
                pass
            
            try:
                tmp = dsMapRecordIdByName[record[dsfielddictionary.dsObjectName2Index]]
            except:
                dsMapRecordIdByName[record[dsfielddictionary.dsObjectName2Index]] = int(record[dsfielddictionary.dsRecordIdIndex])
                pass
            
            try:
                dsMapTypeByRecordId[int(record[dsfielddictionary.dsRecordIdIndex])] = record[dsfielddictionary.dsObjectTypeIdIndex]
            except:
                sys.stderr.write("\n[!] Warning! Error at dsMapTypeByRecordId!\n")
                pass
            
            try:
                tmp = dsMapChildsByRecordId[int(record[dsfielddictionary.dsRecordIdIndex])]
            except KeyError:
                dsMapChildsByRecordId[int(record[dsfielddictionary.dsRecordIdIndex])] = []
                pass
            
            try:
                dsMapChildsByRecordId[int(record[dsfielddictionary.dsParentRecordIdIndex])].append(int(record[dsfielddictionary.dsRecordIdIndex]))
            except KeyError:
                dsMapChildsByRecordId[int(record[dsfielddictionary.dsParentRecordIdIndex])] = []
                dsMapChildsByRecordId[int(record[dsfielddictionary.dsParentRecordIdIndex])].append(int(record[dsfielddictionary.dsRecordIdIndex]))
                
        lineid += 1
    sys.stderr.write("\n")
    
    offlid = open(path.join(workdir, "offlid.map"), "wb")
    pickle.dump(dsMapOffsetByLineId, offlid)
    offlid.close()
    
    lidrid = open(path.join(workdir, "lidrid.map"), "wb")
    pickle.dump(dsMapLineIdByRecordId, lidrid)
    lidrid.close()
    
    ridname = open(path.join(workdir, "ridname.map"), "wb")
    pickle.dump(dsMapRecordIdByName, ridname)
    ridname.close()
    
    typerid = open(path.join(workdir, "typerid.map"), "wb")
    pickle.dump(dsMapTypeByRecordId, typerid)
    typerid.close()
    
    childsrid = open(path.join(workdir, "childsrid.map"), "wb")
    pickle.dump(dsMapChildsByRecordId, childsrid)
    childsrid.close()
    
    pek = open(path.join(workdir, "pek.map"), "wb")
    pek.write(dsfielddictionary.dsEncryptedPEK)
    pek.close()
    
    dsBuildTypeMap(dsDatabase, workdir)

def dsBuildTypeMap(dsDatabase, workdir):
    global dsMapTypeIdByTypeName

    schemarecid  = -1
    
    i = 0
    l = len(dsMapLineIdByRecordId)
    for recordid in dsMapLineIdByRecordId:
        sys.stderr.write("\r[+] Searching for Schema object - %d%% -> %d records processed" % (
                                            i*100/l,
                                            i+1
                                            ))
        lineid = int(dsMapLineIdByRecordId[recordid])
        offset = int(dsMapOffsetByLineId[lineid])
        dsDatabase.seek(offset)
        line = ""
        record = ""
        line = dsDatabase.readline()
        record = line.split('\t')
        name = record[dsfielddictionary.dsObjectName2Index]
        oc = record[dsfielddictionary.dsObjectColIndex]
        if name == "Schema" and oc == "1":
            schemarecid = recordid
            break
        i += 1

    sys.stderr.write("\r[+] Searching for Schema object - %d%% -> %d records processed" % (
                                            100,
                                            i
                                            ))
    sys.stderr.write("\n")
    sys.stderr.flush()

    schemachilds = dsMapChildsByRecordId[schemarecid]
    i = 0
    l = len(schemachilds)
    for child in schemachilds:
        sys.stderr.write("\r[+] Extracting schema information - %d%% -> %d records processed" % (
                                            i*100/l,
                                            i+1
                                            ))
        sys.stderr.flush()
        lineid = int(dsMapLineIdByRecordId[int(child)])
        offset = int(dsMapOffsetByLineId[int(lineid)])
        dsDatabase.seek(offset)
        
        record = ""
        line = ""
        line = dsDatabase.readline()
        if line != "":
            record = line.split('\t')
            name = record[dsfielddictionary.dsObjectName2Index]
            dsMapTypeIdByTypeName[name] = child
        i += 1
    
    typeidname = open(path.join(workdir, "typeidname.map"), "wb")
    pickle.dump(dsMapTypeIdByTypeName, typeidname)
    typeidname.close()
    
    sys.stderr.write("\r[+] Extracting schema information - %d%% -> %d records processed" % (
                                            100,
                                            i
                                            ))
    sys.stderr.write("\n")
    sys.stderr.flush()

def dsInitEncryption(syshive_fname):
    bootkey = get_syskey(syshive_fname)
    enc_pek = unhexlify(dsfielddictionary.dsEncryptedPEK[16:])
    dsfielddictionary.dsPEK=dsDecryptPEK(bootkey, enc_pek)
