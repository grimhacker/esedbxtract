'''
        .1111...          | Title: command
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | executes a command as a subprocess
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
--------------------------------------------------------------------------------
Created on 22 Sep 2013
@author: GrimHacker
Part of esedbxtract.
esedbxtract extracting hashes from ntds.dit and system files.
    Copyright (C) 2014  Oliver Morton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import logging

from subprocess import CalledProcessError
from async_subprocess import AsyncPopen, PIPE


class Command(object):
    def __init__(self):
        try:
            self.log = logging.getLogger(__name__)
        except Exception as e:
            print "problem setting up log. {0}".format(e)
            self.log = None
        self._returncode = None

    def _get_returncode(self):
        """Return the return code of the executed command."""
        return self._returncode

    def _stdout(self, out):
        """print line from stdout of executed command."""
        for line in out.split("\n"):
            if line != "":  # output anything that isn't a blank line
                self.log.info("{0}".format(line))

    def _stderr(self, err):
        """Print line from stderr of executed command."""
        for line in err.split("\n"):
            if line != "":  # output anything that isn't a blank line
                self.log.warning("{0}".format(line))

    def _execute(self, cmd):
        """Run the specified command as a subprocess."""
        self.log.debug("running: '{0}'".format(cmd))
        try:
            proc = AsyncPopen(cmd, stdout=PIPE, stderr=PIPE)
        except CalledProcessError, e:
            self.log.error("{0}: {1}".format(e.errno, e.strerror))
            #return "{0}: {1}".format(e.errno, e.strerror)
        else:
            while proc.poll() is None:  # while subprocess hasn't finished
                out, err = proc.communicate("s")
                if err is not None:
                    self._stderr(err)
                if out is not None:
                    self._stdout(out)
                #line = proc.stdout.readline().strip("\n")
                #self._stdout(line)
                # line = proc.stderr.readline().strip("\n")  # waits for stderr. # TODO: need to put this in a thread
                # self._stderr(line)
            # when we get to here the subprocess has finished running
        self._returncode = proc.returncode
